class DefaultSectionConfigData(object):
    """
    Contains the configuration data found inside the "default" section of .CONF file
    The data is stored in appropriate attributes with meaningful names with convenient get/ set methods
    """
    def __init__(self, config):
        '''
        Configuration data found inside the "default" section.

        :param boolean is_daemon: should I run the application as a deamon
        '''

        # Attributes defined inside the "default" section
        # -----------------------------------------------

        # Delete all from Redis
        self._start_up_fresh_db = config.getboolean("default",
                                                    "start-up-fresh-db")

        # Run application as Linux daemon
        self._is_daemon = config.getboolean("default", "is-daemon")

        # Logging specific configuration data
        self._log_full_filename = config.get("default", "log-full-file-name")
        self._log_level = config.get("default", "log-level")
        self._log_msg_format = config.get("default", "log-msg-format")
        self._log_max_bytes = config.getint("default", "log-max-bytes")
        self._log_backup_count = config.getint("default", "log-backup-count")

        # Scheduler - frequency of different scans
        self.hosts_scan_freq = config.get("default", "hosts-scan-freq")
        self.ports_scan_freq = config.get("default", "ports-scan-freq")

    @property
    def start_up_fresh_db(self):
        return self._start_up_fresh_db

    @property
    def is_daemon(self):
        return self._is_daemon

    @property
    def log_full_filename(self):
        return self._log_full_filename

    @property
    def log_level(self):
        return self._log_level

    @property
    def log_msg_format(self):
        return self._log_msg_format

    @property
    def log_max_bytes(self):
        return self._log_max_bytes

    @property
    def log_backup_count(self):
        return self._log_backup_count

    @property
    def scheduler_frequencies(self):
        return {
            "host_scan_freq": self.hosts_scan_freq,
            "ports_scan_freq": self.ports_scan_freq
        }
