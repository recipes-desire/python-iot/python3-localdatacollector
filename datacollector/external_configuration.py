from configparser import ConfigParser, ExtendedInterpolation

from datacollector.default_section_config_data import DefaultSectionConfigData
from datacollector.nmap_config_data import NMapConfigData
from datacollector.redismq_config_data import RedisMQConfigData
from datacollector.mqttbroker_config_data import MQTTBrokerConfigData


class FileConfigurator(object):
    """
    Use this class for application external configuration.
    It is responsible for reading .CONF file in /etc folder and assigning the external data to 
    approprate variables. 
    Before establishing a conenction with supporting servers like Redis adn MQTT-Broker the IPs and 
    Ports must be read from external configuration.
    """
    def __init__(self, full_pathname="/etc/datacollector/datacollector.conf"):
        ''' 
        Define the external file containg the aplication configuration data
        :param str config_path: The full path name of the .CONF file
        '''
        self.full_pathname = full_pathname

    def read_config_data(self):
        '''
        After full-pathname of .CONF file is defined proceed with reading the contents.
        '''
        config = ConfigParser(interpolation=ExtendedInterpolation())
        dataset = config.read(self.full_pathname)

        if len(dataset) == 0:
            # Problem with CONF file
            raise Exception("Problem with CONF file ... No Data Read !")

        self.config = config

    def parse_sections(self):
        '''
        The ORM mapping .. read the config-data and assign it to appropriate variables.
        Common variables are enclosed in classes with meaningful names, like RedisConfigData
        '''
        sections = self.config.sections()

        if len(sections) == 0:
            # No Sections Found inside .CONF
            raise Exception("Wrong Format of .CONF ... No Sections Found !")

        for _section in sections:
            # For each section @ CONF file

            #print( "Current Section is .. {}".format(_section) )
            #pipis = self.config.get( _section, "pipis" )

            self.map_section_to_class(str(_section))

    def map_section_to_class(self, section_name):
        '''
        Save to configuration data to appropriate classes for easy access and handling.
        '''

        if section_name.lower() == "default":
            # Reading DEFAULT section
            self._default_config_data = DefaultSectionConfigData(self.config)
        elif section_name.lower() == "nmap-net":
            self._nmap_config_data = NMapConfigData(self.config)
        elif section_name.lower() == "redis-mq":
            self._redismq_config_data = RedisMQConfigData(self.config)
        elif section_name.lower() == "mqttbroker-local":
            self._mqttbroker_local = MQTTBrokerConfigData(self.config,
                                                          is_local=True)
        elif section_name.lower() == "mqttbroker-central":
            self._mqttbroker_central = MQTTBrokerConfigData(self.config,
                                                            is_local=False)
        else:
            raise Exception("Unknown Section Name %s", section_name)

    @property
    def default_config_data(self):
        return self._default_config_data

    @property
    def nmap_config_data(self):
        return self._nmap_config_data

    @property
    def redismq_config_data(self):
        return self._redismq_config_data

    @property
    def mqttbroker_local(self):
        return self._mqttbroker_local

    @property
    def mqttbroker_central(self):
        return self._mqttbroker_central
