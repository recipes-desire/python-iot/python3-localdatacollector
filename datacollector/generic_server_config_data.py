class GenericServerConfigData(object):
    """
    This class is a data store ... Important configuration parameters for TCP-Server connection
    """
    def __init__(self, config, section):
        '''
        Read important parameters for TCP-Server connection ... IP and Port

        :param ConfigParser config: a configuration parser for an external CONF file
        '''

        self._ip = config.get(section, "ip")
        self._port = config.getint(section, "port")

    @property
    def ip(self):
        return self._ip

    @property
    def port(self):
        return self._port
