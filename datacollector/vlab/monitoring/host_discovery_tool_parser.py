from tcpclientserver.server import SEPARATOR, RESPONSE_TERMINATION_CHARS

from .host_discovery_book import HostDiscoveryBook

#from . import *

from datacollector.main import logger


class HostDiscoveryToolParser(object):
    def __init__(self, nmap_str):
        self.nmap_str = nmap_str

    def from_string_to_list(self):
        # From String -> List
        self.hosts_up = self.nmap_str.split(SEPARATOR)
        return self.hosts_up

    #@pysnooper.snoop()
    def remove_first_characters(self, starting_str):
        length = len(starting_str)

        self.host_name_ip = list()
        for host in self.hosts_up:
            if host.startswith(starting_str):
                # Nmap Response for Host HostDiscovery
                host = host.strip()
                part_name_ip = host[length:]

                logger.debug("Name + IP is ... %s", part_name_ip)
                self.host_name_ip.append(part_name_ip)

    def split_remaining_name_ip(self):
        book = HostDiscoveryBook()
        for name_ip in self.host_name_ip:
            name, ip = name_ip.split()

            if ip.endswith(RESPONSE_TERMINATION_CHARS):
                # the end of the message
                l = len(RESPONSE_TERMINATION_CHARS)
                ip = ip[0:len(ip) - l]

            # Remove parentheses from nmap returned IP
            ip = ip.replace("(", "")
            ip = ip.replace(")", "")

            logger.debug("Name is ... %s && IP is %s !", name, ip)

            book.append_host_up(name, ip)

        return book
