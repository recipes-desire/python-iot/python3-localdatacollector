class HostDiscoveryBook(object):
    def __init__(self):
        self.hosts_up = list()

    def append_host_up(self, name, ip):
        self.hosts_up.append({"name": name, "ip": ip})

    def num_of_entries(self):
        return len(self.hosts_up)

    def __str__(self):
        my_message = ""
        for host in self.hosts_up:
            my_message += host["name"] + " ... " + host["ip"] + " , "

        return my_message

    def __iter__(self):
        return self.hosts_up.__iter__()

    def __next__(self):
        return self.hosts_up.__next__()

    def ips_to_array(self):
        # Use LIST-COMPREHENSION
        ips_arr = [item["ip"] for item in self.hosts_up]

        return ips_arr

    def to_array(self):
        return self.hosts_up
