from datacollector.generic_server_config_data import GenericServerConfigData


class MQTTBrokerConfigData(GenericServerConfigData):
    """
    Contains all necessary data to connect with an MQTT-Broker and send messages.
    """
    def __init__(self, config, is_local):
        '''
        :param ConfigParser config: file parser for reading data from CONF file 
        :param boolean is_local: true if local broker running at board
        '''

        if is_local:
            # Local MQTT-Broker Configuration
            super(MQTTBrokerConfigData, self).__init__(config,
                                                       "mqttbroker-local")
        else:
            # Central/ Server-Side MQTT-Broker Configuration
            super(MQTTBrokerConfigData, self).__init__(config,
                                                       "mqttbroker-central")
