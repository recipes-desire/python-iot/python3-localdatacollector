import logging

from logging.handlers import RotatingFileHandler

from datacollector.default_section_config_data import DefaultSectionConfigData

LEVELS = {
    "DEBUG": logging.DEBUG,
    "ERROR": logging.ERROR,
    "INFO": logging.INFO,
}

NAME = "datacollector"


class LocalLogger(object):
    def __init__(self, default_section):
        self._full_filename = default_section.log_full_filename
        self._level = default_section.log_level.upper()

        self._max_bytes = default_section.log_max_bytes
        self._backup_count = default_section.log_backup_count

        self._msg_format = default_section.log_msg_format

    def setup(self):
        self._logger = logging.getLogger(NAME)

        # Do NOT propagate messages to console
        self._logger.propagate = False

        self._logger.setLevel(LEVELS[self._level])

        fh = RotatingFileHandler(filename=self._full_filename,
                                 maxBytes=self._max_bytes * 1024 * 1024,
                                 backupCount=self._backup_count)

        formatter = logging.Formatter(self._msg_format)
        fh.setFormatter(formatter)

        self._logger.addHandler(fh)

    @property
    def logger(self):
        return self._logger
