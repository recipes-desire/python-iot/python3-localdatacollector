logger = None


def run_externalConfigurationDemo():
    '''
    A helper function to call External-Configuration for testing purposes.

    :param string full_pathname: the .CONF file to read {folder + name}
    '''
    global logger

    from datacollector.external_configuration import FileConfigurator
    file_configurator = FileConfigurator()
    file_configurator.read_config_data()
    file_configurator.parse_sections()
    default_config_data = file_configurator.default_config_data

    from datacollector.local_logger import LocalLogger
    localLogger = LocalLogger(default_config_data)
    localLogger.setup()

    # *****************************
    logger = localLogger.logger
    # *****************************

    logger.info("Welcome to Badlands .. Ready to Meet the Barons ?")

    # NMAP TCP-Agent
    nmap_config_data = file_configurator.nmap_config_data
    logger.info("NMAP Connection ... %s: %d", nmap_config_data.ip,
                nmap_config_data.port)

    # Redis message-queue
    redismq_config_data = file_configurator.redismq_config_data
    logger.info("RedisMQ Connection ... %s: %d", redismq_config_data.ip,
                redismq_config_data.port)

    # MQTT-Broker Local
    mqttbroker_local = file_configurator.mqttbroker_local
    logger.info("MQTTBroker-Local Connection ... %s: %d", mqttbroker_local.ip,
                mqttbroker_local.port)

    # MQTT-Broker Central
    mqttbroker_central = file_configurator.mqttbroker_central
    logger.info("MQTTBroker-Central Connection ... %s: %d",
                mqttbroker_central.ip, mqttbroker_central.port)

    # Scheduler Frequencies
    scheduler_frequencies = default_config_data.scheduler_frequencies
    logger.info("Scheduler Host & Port Frequencies ... %s",
                scheduler_frequencies)

    is_daemon = default_config_data.is_daemon
    if is_daemon == True:
        logger.debug("Run Application as Daemon !")
        from datacollector.daemon_tools import daemon_call
        daemon_call(redismq_config_data, nmap_config_data)
    else:
        from datacollector.control_unit import main
        main(redismq_config_data, nmap_config_data, mqttbroker_local,
             scheduler_frequencies)
