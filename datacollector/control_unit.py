import datetime

#from . import *

from .scheduler.redis_communication_manager import check_hosts_up_existence, store_open_ports, get_all_hosts_up_from_datastore, store_hosts_up

from .scheduler.host_discovery_request import perform_host_discovery

from .scheduler.port_scanning_request import perform_port_scanning
from .scheduler.simple_scheduler import scheduler_main

from datacollector.main import logger

#from configparser import ConfigParser
#config = ConfigParser()
#config.read("/etc/datacollector/datacollector.conf")

from datacollector.scheduler.redis_communication_manager import set_open_ports_of_server, get_open_ports_of_server_from_datastore, store_hosts_up, delete_net_data_from_local_store, get_list_of_keys_in_db, check_datastore

from datacollector.scheduler.ping_pong_test import check_nmap_agent_listening
from datacollector.scheduler.mqttbroker_expert import check_broker


#####
## Application Entrypoint
###
def main(redismq_config_data, nmapagent_config_data, mqttbroker_config_data,
         scheduler_frequencies):
    start_time = datetime.datetime.now()
    logger.debug("Ready to Start Monitoring Data-Collector at ... %s",
                 start_time)

    # Step 1: Check Redis Availability (In case of No Redis Available an Exception is Thrown)
    # ------
    if not check_datastore(
            logger, redismq_config_data.ip, redismq_config_data.port, db=0):
        logger.error(
            "Cannot Find a Datastore for Networking Data ... Aborting !")
        return

    # Step 2: Check NMAP-Agent Connectivity (In case of NO NMAP-Agent is listening an Exception is Thrown)
    # ------
    if not check_nmap_agent_listening(logger, nmapagent_config_data.ip,
                                      nmapagent_config_data.port):
        logger.error("Cannot Communicate with NMAP-Agent ... Aborting !")
        return

    # Step 3: Check MQTT-Broker Availability ... Local & Central (What should we do in case of Broker-Missing)
    # ------
    if not check_broker(logger, mqttbroker_config_data.ip,
                        mqttbroker_config_data.port):
        logger.error("Cannot Communicate with MQTT-Broker ... Aborting !")
        return

    #return

    # ************************************
    # Should I clean the datastore first ?
    # ************************************
    start_with_fresh_datastore = redismq_config_data.start_with_fresh
    if start_with_fresh_datastore:
        # Delete old content
        delete_net_data_from_local_store()

    #set_open_ports_of_server()
    #logger.debug( "----> %s", get_open_ports_of_server() )

    ### EndOF Step 1 !!!

    # Step 2: Let the application check wether ...
    # ------
    #    --> the "HostsUp" data is stored at Redis
    #    --> in case of no HostsUp data ... first execution cycle & data must gathered first
    if check_hosts_up_existence():
        # ###
        # HostsUp Found in Redis !
        logger.debug(
            "NMap already performed Host-Discovery ... Stored Results Found")

        hosts_book = get_all_hosts_up_from_datastore()
        logger.debug("Found in Datastore Hosts-Up ... %s", hosts_book)
    else:
        # ###
        # No HostsUp found in Redis .. Need to send Immediately Nmap request
        logger.debug(
            "No Host-Discovery Results stored ... Sending Immediately Request to NMAP"
        )
        hosts_book = perform_host_discovery(nmapagent_config_data.ip,
                                            nmapagent_config_data.port)

        store_hosts_up(hosts_book)
    ### EndOF Step 2: Hosts-Up {} are available

    # Step 3: AFter having a long list of hosts_up, it is time to examine open-ports in each of the servers
    # ------
    ports_book = perform_port_scanning(logger, nmapagent_config_data.ip,
                                       nmapagent_config_data.port)

    store_open_ports(ports_book)
    get_list_of_keys_in_db()

    # Start scheduler
    scheduler_main(logger, scheduler_frequencies, nmapagent_config_data)
