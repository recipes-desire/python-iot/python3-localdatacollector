from messageqclient import *
from messageqclient.constants import KeyNames

from . import *

logger = None


def check_datastore(loggerIn, host, port, db):
    global logger
    logger = loggerIn

    is_available = check_datastore_availability(logger, host, port, db)
    return is_available

    #return True


"""
In Datastore , propably a Redis instance, there is a list containing hosts_up on local network.
In case of fresh run and empty datastore ... avoid querying for already stored.     
"""


def check_hosts_up_existence():
    key = KeyNames().hostsup
    return True if check_key_existence(key) == 1 else False


def delete_net_data_from_local_store():
    global logger
    response = delete_data_from_db(logger)
    logger.debug("Response of DB-Flush Request: %s", response)


def get_all_hosts_up_from_datastore():
    key = KeyNames().hostsup
    values = get_all_values_from_set(key)

    # Redis response contains b'my-string'
    #return values
    return [v.decode() for v in values]


def get_open_ports_of_server_from_datastore(server_key="dummyserver"):
    #open_ports = get_all_values_from_set( server_key )
    open_ports = get_open_ports_for_server(server_key)

    return open_ports


def set_open_ports_of_server(server_key="dummyserver", ports_list=[1, 2]):
    append_end(server_key, ports_list)


def store_hosts_up(hosts_book):
    key = KeyNames().hostsup
    values = hosts_book.ips_to_array()
    #logger.debug("Ready to Store Hosts: %s", values)

    #key = "pipis"
    #values = ["1","2"]
    append_start(key, values)


def store_open_ports(ports_book):
    #logger.debug( "Ports-Book First Page(s) ... %s", ports_book[0] )

    for record in ports_book:
        # Server && Open-Ports
        examined_server = record["server"].strip()

        if examined_server.endswith("@endofresponse@@"):
            # Last server in NMAP response for active ones
            examined_server = examined_server.replace("@endofresponse@@", "")

        open_ports_l = record["ports"].strip()

        #logger.debug("--> %s ... %s", examined_server, open_ports_l)
        save_open_ports_for_server(examined_server, open_ports_l)

    return


def get_list_of_keys_in_db():
    # Get a list of all my keys
    all_keys = my_keys_list()

    logger.debug("List of Stored Keys in DB --> \n%s", all_keys)
