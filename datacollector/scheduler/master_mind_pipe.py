from .redis_communication_manager import get_all_hosts_up_from_datastore
from .redis_communication_manager import get_open_ports_of_server_from_datastore

from mastermind import *

# Import ... Logger
#from . import *

from datacollector.main import logger
"""
Validate and Compare the list of ... Currently Active Servers VS Stored  
"""


def is_server_down(hosts_up_collected_now):
    # Call an AI entitie .... class ValidateCollectedData .. from MasterMind subproject
    hosts_up_stored = get_all_hosts_up_from_datastore()
    if not hosts_up_stored:
        # Empty results set from redis
        logger.error("Aborting Validation of Hosts-Up ... Empty Stored")
        return

    if not hosts_up_collected_now:
        # Problem with Collected data ... Empty result set
        logger.error("Aborting Validation of Hosts-Up ... Empty Collected")
        return

    hosts_up_ips_arr = hosts_up_collected_now.ips_to_array()

    ValidateCollectedData(
        logger, hosts_up_ips_arr, hosts_up_stored,
        ComparisonRulesNames.EQUAL).compare_collected_stored()


"""
NMap collected new data for open-ports during the latest run cycle.
Must be validated and compared to stored data ... need decisions-made for ports-closed.
"""


def is_port_closed(port_scan_book):
    logger.debug("Datapipe for Master-Mind: Validate Latest Open Ports Found")

    # Get a list of Stored HostsUp
    hostsUp = get_all_hosts_up_from_datastore()

    #logger.debug( "Get-All-Hosts-Up-From-Datastore() --> %s", hostsUp )

    # Get OpenPorts for every HostUp
    for host in hostsUp:
        #logger.debug("Host-Up from Datastore --> %s", host )
        openPorts = get_open_ports_of_server_from_datastore(host)
        #logger.debug("Open-Ports of Server from Datastore --> %s", openPorts)

    #get_stored_hosts_up()
    #ValidateCollectedData( port_scan_book ).compare_collected_stored()
