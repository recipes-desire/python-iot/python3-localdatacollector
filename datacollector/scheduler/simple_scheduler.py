import schedule
import time
import os

logger = None
nmap_wrapper_ip = None
nmap_wrapper_port = None

from .host_discovery_request import perform_host_discovery
from .port_scanning_request import perform_port_scanning
"""
Search for Hosts-Up using a loose schedule
"""


def simple_job_host():
    global nmap_wrapper_ip, nmap_wrapper_port

    perform_host_discovery(nmap_wrapper_ip, nmap_wrapper_port)


"""
Search for Open-Ports using a frequent time scheme 
"""


def simple_job_port():
    global logger
    global nmap_wrapper_ip
    global nmap_wrapper_port

    perform_port_scanning(logger, nmap_wrapper_ip, nmap_wrapper_port)


def simple_test(freq_host_value, freq_host_units, freq_port_value,
                freq_port_units):
    # PRINT() ... It is not working in case of subprocess-context
    #print("Simple Scheduler Land")
    if (freq_host_units == "MINUTES"):
        logger.debug(
            "Ready to Schedule Frequent Host-Discovery .... Every %d minute(s)",
            freq_host_value)
        schedule.every(freq_host_value).minutes.do(simple_job_host)
    elif (freq_host_units == 'HOURS'):
        logger.debug(
            "Ready to Schedule Infrequent Host-Discovery .... Every %d hour(s)",
            freq_host_value)
        schedule.every(freq_host_value).hours.do(simple_job_host)
    else:
        raise NotImplementedError(
            "Try Another Unit for Frequency of Host Discovery")

    if (freq_port_units == "MINUTES"):
        logger.debug(
            "Ready to Schedule Frequent Port-Scanning .... Every %d minute(s)",
            freq_port_value)
        schedule.every(freq_port_value).minutes.do(simple_job_port)
    elif (freq_port_units == 'HOURS'):
        logger.debug(
            "Ready to Schedule Infrequent Port-Discovery .... Every %d hour(s)",
            freq_port_value)
        schedule.every(freq_port_value).hours.do(simple_job_port)
    else:
        raise NotImplementedError(
            "Try Another Unit for Frequency of Port Scanning")

    while True:
        schedule.run_pending()
        time.sleep(1)


def scheduler_main(_logger, scheduler_config_data, _nmap_wrapper):
    global logger
    logger = _logger

    global nmap_wrapper_ip
    nmap_wrapper_ip = _nmap_wrapper.ip

    global nmap_wrapper_port
    nmap_wrapper_port = _nmap_wrapper.port

    logger.debug("Crossing Scheduler's Gate")

    #HOST_DISCOVERY_FREQ = os.environ[ 'HOST-DISCOVERY-FREQ']
    HOST_DISCOVERY_FREQ = scheduler_config_data['host_scan_freq']
    freq_host = HOST_DISCOVERY_FREQ.split("-")
    freq_host_value = int(freq_host[0])
    freq_host_units = str(freq_host[1])

    #PORT_SCANNING_FREQ = os.environ[ 'PORT-SCANNING-FREQ']
    PORT_SCANNING_FREQ = scheduler_config_data['ports_scan_freq']
    freq_port = PORT_SCANNING_FREQ.split("-")
    freq_port_value = int(freq_port[0])
    freq_port_units = str(freq_port[1])

    logger.debug("Host-disc-freq = %s - %s ... Port-scan-freq = %s - %s",
                 freq_host[0], freq_host[1], freq_port_value, freq_port_units)

    simple_test(freq_host_value, freq_host_units, freq_port_value,
                freq_port_units)
