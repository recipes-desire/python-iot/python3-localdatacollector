#import pysnooper

import os

from tcpclientserver.client import send_request
from datacollector.vlab.monitoring.port_scan_response_parser import PortScanResponseParser

from datacollector.vlab.monitoring.port_scan_book import PortScanBook
from datacollector.vlab.monitoring.host_discovery_book import HostDiscoveryBook

from .redis_communication_manager import get_all_hosts_up_from_datastore
from .master_mind_pipe import is_port_closed

#from . import *

logger = None
nmap_wrapper_ip = None
nmap_wrapper_port = None

MSG_PORT_SCAN = "nmap:port:scan"


def validate_port_scan_data(book):
    try:
        is_port_closed(book)
    except Exception as x:
        logger.debug("Problem with Data Validation --> %s", x)


def perform_port_scanning(_logger, _nmap_wrapper_ip, _nmap_wrapper_port):
    global logger
    logger = _logger
    global nmap_wrapper_ip
    nmap_wrapper_ip = _nmap_wrapper_ip
    global nmap_wrapper_port
    nmap_wrapper_port = _nmap_wrapper_port

    hosts_book = get_all_hosts_up_from_datastore()
    port_scan_book = perform_port_scanning_for_servers(hosts_book)

    validate_port_scan_data(port_scan_book)

    # Bad Practice : Only used when NO data found in Redis ... First scan local nertwork with nmap and after save
    return port_scan_book


"""
Having a list o Hosts_Up {} and needing to examine each server for open-ports.
"""


#@pysnooper.snoop()
def perform_port_scanning_for_servers(servers_book):
    # Send Request to NMap-Agent using a TCP-Socket
    #global port_scan_book
    port_scan_book = PortScanBook()

    for server in servers_book:
        open_ports_msg = ""
        if isinstance(servers_book, HostDiscoveryBook):
            open_ports_msg = create_port_scanning_request(
                nmap_wrapper_ip, nmap_wrapper_port, server["ip"],
                MSG_PORT_SCAN)
        else:
            open_ports_msg = create_port_scanning_request(
                nmap_wrapper_ip, nmap_wrapper_port, server, MSG_PORT_SCAN)

        if not open_ports_msg:
            # No response-message for open-ports from NMAP
            raise Exception("Empty Response from NMap for Open-Ports")

        # Parse the NMap response ...
        parser = PortScanResponseParser(open_ports_msg)
        # Which Server & Ports
        server, ports = parser.run()

        port_scan_book.add_server_with_open_ports(server=server, ports=ports)

        logger.debug("***** Open Ports Book: +=%s [%s] ... #%s", server,
                     len(ports), port_scan_book)

    return port_scan_book


"""
Send a Port-Scanning Request for a Server in Hosts-Up list.
"""


def create_port_scanning_request(nmap_agent, nmap_port, host_up,
                                 port_scanning_REQ):
    complex_msg = port_scanning_REQ + ":" + host_up + ":!"
    open_ports_msg = send_request(logger, nmap_agent, nmap_port, complex_msg)

    return open_ports_msg
