from time import sleep
from daemonize import Daemonize

pid = "/tmp/test.pid"

redismq_config_data = None


def daemon_main():
    '''
    Endless loop ... wake up every 5 msecs
    '''
    global redismq_config_data

    from control_unit import main
    main(redismq_config_data)


def daemon_call(redismq_config_dataIn, nmapagent_config_dataIn):
    global redismq_config_data
    redismq_config_data = redismq_config_dataIn

    app_daemon = Daemonize(app="test_app", pid=pid, action=daemon_main)
    app_daemon.start()
