from setuptools import setup

install_requires = [
    'daemonize==2.4.7', 'messageqclient==1.0.0', 'tcpclientserver==1.0.0',
    'mqttbrokerclient==2.8.4', 'mastermind==1.0.0'
]

#install_requires = [
#    'schedule==0.6.0',
#    'daemonize==2.4.7'
#]

# only python-daemoize 2.4.7 available, no python-3 -> fixed: implemented python3-daemoize_2.4.7
# @@@TODO: schedule not available
# @@@TODO: what about our own dependencies here? -> fixed ?

setup(
    name='datacollector',
    version='1.0.0',
    description='Application that collects local network performance data',
    long_description='Not given yet !',
    author='Miltos Vimplis',
    url=
    'https://gitlab.com/recipes-desire/python-iot/python3-localdatacollector',
    license='MIT license',
    packages=[
        'datacollector',
        'datacollector.scheduler',
        'datacollector.vlab',
        'datacollector.vlab.monitoring',
    ],
    #package_data=[],
    entry_points={
        'console_scripts': [
            'datacollector=datacollector.control_unit:main',
            'datacollector-demoExternalConfig=datacollector.main:run_externalConfigurationDemo'
        ]
    },
    install_requires=install_requires,
    #extras_require={},
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Programming Language :: Python :: 3.4',
        'Topic :: System :: Network :: Monitoring',
    ],
    zip_safe=True)
