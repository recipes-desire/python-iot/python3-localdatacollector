==  update from developer and push upstream ==
>> git co master

hack the .gitconfig file - see sample 

I added this:

[remote "miltos-master"]
        url = git@gitlab.com:poky_noty/recipes-desire/python-iot/python3-localdatacollector.git
        fetch = +refs/heads/*:refs/remotes/miltos-master/*

>> git remote -v
miltos-master   git@gitlab.com:poky_noty/recipes-desire/python-iot/python3-localdatacollector.git (fetch)
miltos-master   git@gitlab.com:poky_noty/recipes-desire/python-iot/python3-localdatacollector.git (push)
origin  git@gitlab.com:recipes-desire/python-iot/python3-localdatacollector.git (fetch)
origin  git@gitlab.com:recipes-desire/python-iot/python3-localdatacollector.git (push)

>> git fetch miltos-master
remote: Enumerating objects: 13, done.
remote: Counting objects: 100% (13/13), done.
remote: Compressing objects: 100% (9/9), done.
remote: Total 9 (delta 6), reused 0 (delta 0)
Unpacking objects: 100% (9/9), done.
From gitlab.com:poky_noty/recipes-desire/python-iot/python3-localdatacollector
 * [new branch]      master     -> miltos-master/master
 * [new branch]      miltos-master -> miltos-master/miltos-master

>> git branch -a
* master
  remotes/miltos-master/master
  remotes/miltos-master/miltos-master
  remotes/origin/master

>> git branch master-merge-branch

>> git co master-merge-branch

>> git branch

>> git diff master

>> git diff master-merge-branch miltos-master/miltos-master

check here if changes are acceptable

>> git rebase miltos-master/miltos-master
First, rewinding head to replay your work on top of it...
Fast-forwarded master-merge-branch to miltos-master/miltos-master.

>> git diff master

... diff like above ...

>> git co master

>> git rebase master-merge-branch

>> git branch -d master-merge-branch

Now there are no more differences !

in case there are more local changes (e.g. files which are not under version control)
you can add/commit them now

>> ./scripts/push-to-gitlab.sh
